<?php include 'header.php'; ?>
<section class="breadcrumb" style=" background: url('assets/images/breadcrumbs.jpg') center ;">
    <div class="thin_layer" style="background: #000; opacity: 0.7"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-sm-12 col-xs-12 breadcrumb_wrapper">
                <h1 class="entry-title">About Us</h1>
                <nav id="breadcrumb" class="fitness-park-breadcrumb">
                    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"
                         itemprop="breadcrumb">
                        <ul class="trail-items" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <meta name="numberOfItems" content="2">
                            <meta name="itemListOrder" content="Ascending">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-begin"><a href="#" rel="home"
                                                                  itemprop="item"><span itemprop="name">Home</span></a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-end"><span itemprop="item"><span itemprop="name">About Us</span></span>
                                <meta itemprop="position" content="2">
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_aboutus_front">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6  col-sm-12">
                <h2 class="section-title">
                    Better Solutions For Borrowers <br> <span>And Investors In Middle Market Credit</span>
                </h2>
                <p>
                    Nulla ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus. Aliquam scelerisque matt.
                    Morbi id dolor turpis. Prae sent volutpat scelerisque lectus a condimentum. Vestibulum sit amet
                    fermentum lacus.<br><br>Nulla ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus.
                    Aliquam scelerisque matt.
                    Morbi id dolor turpis. Praesent volutpat scelerisque lectus a condimentum. Vestibulum sit amet
                    fermentum lacus.
                </p>
            </div>

            <div class="col-lg-7 col-md-6 col-sm-12 ">
                <img src="assets/images/about-1.png" alt="" width="670" height="500">
            </div>
        </div>
    </div>
</section>

<section class="call_to_action">
    <div class="thin_layer" style="background: #000; opacity: 0.7"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-12">
                <div class="call_to_action_inner">
                    <h2>Ready To Work with Our Company?</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in condimentum risus. Fusce
                        rutrum, leo in elementum sodales, magna eros vehicula ante, eget semper mi lectus nec ipsum.</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 mt-20">
                <a href="" class="link btn_yellow ">Call Us Now</a>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_progres_bar_skill">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-12 ">
                <h2 class="section-title">
                    Our <span>Skills</span>
                </h2>

                <div class="progres_bar">
                    <div class="skill">
                        <p>Kitchen Renovation</p>
                        <div class="skill-bar wow slideInLeft animated" style="width: 95%">
                            <span class="skill-count">95%</span>
                        </div>
                    </div>
                    <div class="skill">
                        <p>Home</p>
                        <div class="skill-bar  wow slideInLeft animated" style="width: 85%">
                            <span class="skill-count">85%</span>
                        </div>
                    </div>
                    <div class="skill">
                        <p>office</p>
                        <div class="skill-bar  wow slideInLeft animated" style="width: 90%">
                            <span class="skill-count">90%</span>
                        </div>
                    </div>
                    <div class="skill">
                        <p>Swimming Pool</p>
                        <div class="skill-bar  wow slideInLeft animated" style="width: 96%">
                            <span class="skill-count">96%</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 ">
                <h2 class="section-title">
                    Frequently Asked <span>Question</span>
                </h2>

                <div id="accordion">
                    <h3>Section 1</h3>
                    <div>
                        <p>
                            Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
                            ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
                            amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
                            odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                        </p>
                    </div>
                    <h3>Section 2</h3>
                    <div>
                        <p>
                            Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
                            purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
                            velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
                            suscipit faucibus urna.
                        </p>
                    </div>
                    <h3>Section 3</h3>
                    <div>
                        <p>
                            Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
                            Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
                            ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
                            lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                        </p>
                        <ul>
                            <li>List item one</li>
                            <li>List item two</li>
                            <li>List item three</li>
                        </ul>
                    </div>
                    <h3>Section 4</h3>
                    <div>
                        <p>
                            Cras dictum. Pellentesque habitant morbi tristique senectus et netus
                            et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in
                            faucibus orci luctus et ultrices posuere cubilia Curae; Aenean lacinia
                            mauris vel est.
                        </p>
                        <p>
                            Suspendisse eu nisl. Nullam ut libero. Integer dignissim consequat lectus.
                            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per
                            inceptos himenaeos.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_testimonial" style="background: url('assets/images/testimonial-bg.jpg');">
    <div class="thin_layer" style="background: #000; opacity: 0.7"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="owl-carousel owl-theme testimonial_slider">
                    <div class="item">
                        <div class="client-img">
                            <img src="assets/images/testimonial-1.jpg" alt="" class="img-fluid ">
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                            suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                            consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>

                        <div class="client-text">
                            <h3>John Martin</h3>
                            <h4>Senior Developer</h4>
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-img">
                            <img src="assets/images/testimonial-2.jpg" alt="" class="img-fluid ">
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                            suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                            consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>

                        <div class="client-text">
                            <h3>John Martin</h3>
                            <h4>Senior Developer</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="cons_light_client_logo">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme client_logo">
                    <div class="item">
                        <img src="assets/images/client-logo-1.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-2.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-3.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-4.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-6.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-7.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-8.png" alt="" class="img-fluid ">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php include 'footer.php'; ?>
