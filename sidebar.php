<aside id="secondary" class="widget-area sticky-sidebar">
    <aside id="search-3" class="widget widget_search">
        <form role="search" method="get" class="search-form" action="http://localhost/business-bliss/">
            <label>
                <span class="screen-reader-text">Search for:</span>
                <input type="search" class="search-field" placeholder="Search …" value="" name="s">
            </label>
            <input type="submit" class="search-submit" value="Search">
        </form>
    </aside>
    <aside id="recent-3" class="widget recent-post-widget">
        <h3 class="widget-title">Recent Post</h3>
        <ul>
            <li>
                <div class="post-image">
                    <a href="#">
                        <img src="assets/images/recent-post-1.jpg" alt="recent post" width="80"
                             height="80">
                    </a>
                </div>
                <div class="post-info">
                    <a href="#"><span class="post-title">It showed a lady fitted out with</span></a>
                    <p class="date"><i class="far fa-clock"></i>
                        Dec 31 </p>
                </div>
            </li>
            <li>
                <div class="post-image">
                    <a href="#">
                        <img src="assets/images/recent-post-2.jpg" alt="recent post" width="80"
                             height="80">
                    </a>
                </div>
                <div class="post-info">
                    <a href="#"><span class="post-title">It showed a lady fitted out with</span></a>
                    <p class="date"><i class="far fa-clock"></i>
                        Dec 31 </p>
                </div>
            </li>
        </ul>
    </aside>
    <aside id="meta-3" class="widget widget_meta">
        <h3 class="widget-title">widget</h3>
        <ul>
            <li><a href="http://localhost/business-bliss/wp-admin/">Site Admin</a></li>
            <li>
                <a href="http://localhost/business-bliss/wp-login.php?action=logout&amp;_wpnonce=4d8e0fba56">Log
                    out</a></li>
            <li><a href="http://localhost/business-bliss/feed/">Entries <abbr
                        title="Really Simple Syndication">RSS</abbr></a></li>
            <li><a href="http://localhost/business-bliss/comments/feed/">Comments <abbr
                        title="Really Simple Syndication">RSS</abbr></a></li>
            <li><a href="https://wordpress.org/"
                   title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a>
            </li>
        </ul>
    </aside>
    <aside id="media_gallery-3" class="widget widget_media_gallery">
        <h3 class="widget-title">Gallery</h3>
        <div id="gallery-1" class="gallery galleryid-183 gallery-columns-3 gallery-size-thumbnail">
            <figure class="gallery-item">
                <div class="gallery-icon portrait">
                    <a href="#">
                        <img width="150" height="150" src="assets/images/team-1.png"
                             class="attachment-thumbnail size-thumbnail" alt="">
                    </a>
                </div>
            </figure>
            <figure class="gallery-item">
                <div class="gallery-icon portrait">
                    <a href="#">
                        <img width="150" height="150" src="assets/images/team-2.png"
                             class="attachment-thumbnail size-thumbnail" alt="">
                    </a>
                </div>
            </figure>

            <figure class="gallery-item">
                <div class="gallery-icon portrait">
                    <a href="#">
                        <img width="150" height="150" src="assets/images/team-2.png"
                             class="attachment-thumbnail size-thumbnail" alt="">
                    </a>
                </div>
            </figure>
            <figure class="gallery-item">
                <div class="gallery-icon portrait">
                    <a href="#">
                        <img width="150" height="150" src="assets/images/team-1.png"
                             class="attachment-thumbnail size-thumbnail" alt="">
                    </a>
                </div>
            </figure>
            <figure class="gallery-item">
                <div class="gallery-icon portrait">
                    <a href="#">
                        <img width="150" height="150" src="assets/images/team-2.png"
                             class="attachment-thumbnail size-thumbnail" alt="">
                    </a>
                </div>
            </figure>
            <figure class="gallery-item">
                <div class="gallery-icon portrait">
                    <a href="#">
                        <img width="150" height="150" src="assets/images/team-1.png"
                             class="attachment-thumbnail size-thumbnail" alt="">
                    </a>
                </div>
            </figure>

        </div>
    </aside>
    <aside id="categories-3" class="widget widget_categories">
        <h3 class="widget-title">Category</h3>
        <ul>
            <li class="cat-item cat-item-4"><a
                    href="http://localhost/business-bliss/category/business/">Business</a> (2)
            </li>
            <li class="cat-item cat-item-1"><a
                    href="http://localhost/business-bliss/category/uncategorized/">Uncategorized</a>
                (4)
            </li>
        </ul>
    </aside>
    <aside id="nav_menu-2" class="widget widget_nav_menu">
        <h3 class="widget-title">Menu</h3>
        <div class="menu-menu-1-container">
            <ul id="menu-menu-2" class="menu">
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-18">
                    <a href="http://localhost/business-bliss/">Home</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-175">
                    <a href="http://localhost/business-bliss/about/">About</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-183 current_page_item menu-item-185">
                    <a href="http://localhost/business-bliss/blog/" aria-current="page">Blog</a>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-208">
                    <a href="#">Service</a>
                    <ul class="sub-menu">
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-209">
                            <a href="http://localhost/business-bliss/clean-coding/">Clean Coding</a>
                        </li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-210">
                            <a href="http://localhost/business-bliss/responsive-design/">Responsive
                                Design</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-211">
                            <a href="http://localhost/business-bliss/unique-layout/">Unique
                                Layout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </aside>




</aside>