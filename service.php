<?php include 'header.php'; ?>
<section class="breadcrumb" style=" background: url('assets/images/breadcrumbs.jpg') center ;">
    <div class="thin_layer" style="background: #000; opacity: 0.7"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-sm-12 col-xs-12 breadcrumb_wrapper">
                <h1 class="entry-title">Our Team</h1>
                <nav id="breadcrumb" class="fitness-park-breadcrumb">
                    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"
                         itemprop="breadcrumb">
                        <ul class="trail-items" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <meta name="numberOfItems" content="2">
                            <meta name="itemListOrder" content="Ascending">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-begin"><a href="#" rel="home"
                                                                  itemprop="item"><span itemprop="name">Home</span></a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-end"><span itemprop="item"><span itemprop="name">Our Team</span></span>
                                <meta itemprop="position" content="2">
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <h2 class="section-title">
                    Our <span>Services</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-1.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Home Construction</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-2.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Handyman service</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-3.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Roofing </h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-4.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Development &amp; CMS</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-5.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Kitchen renovation</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Learn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-6.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Office construction</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="cons_light_client_logo">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme client_logo">
                    <div class="item">
                        <img src="assets/images/client-logo-1.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-2.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-3.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-4.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-6.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-7.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-8.png" alt="" class="img-fluid ">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php include 'footer.php'; ?>
