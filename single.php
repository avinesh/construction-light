<?php include 'header.php'; ?>
<section class="breadcrumb" style=" background: url('assets/images/breadcrumbs.jpg') center ;">
    <div class="thin_layer" style="background: #000; opacity: 0.7"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-sm-12 col-xs-12 breadcrumb_wrapper">
                <h1 class="entry-title">Our Team</h1>
                <nav id="breadcrumb" class="fitness-park-breadcrumb">
                    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"
                         itemprop="breadcrumb">
                        <ul class="trail-items" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <meta name="numberOfItems" content="2">
                            <meta name="itemListOrder" content="Ascending">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-begin"><a href="#" rel="home"
                                                                  itemprop="item"><span itemprop="name">Home</span></a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-end"><span itemprop="item"><span itemprop="name">Our Team</span></span>
                                <meta itemprop="position" content="2">
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_blog-list-area">
    <div class="container">

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <div class="blog-list-box">
                    <figure>
                        <img src="assets/images/blog-1.jpg" alt="blog"/>
                    </figure>
                    <div class="blog-list-content">
                        <h4><a href="#">Photoshop CC 2017</a></h4>
                        <div class="blog-list-text-info">
                            <span>By: <span>M S Nawaz</span></span>
                            <span>Date: <span>20.5.15</span></span>
                        </div>
                        <p>There are many variations of sages of Lorem Ipsum available, but the mrity have suffered
                            alteration in some orm, by injected humo ur,There are many but the mri have suffered
                            alteration in some </p>


                    </div>
                </div>
                <nav class="navigation post-navigation" role="navigation">
                    <h2 class="screen-reader-text">Post navigation</h2>
                    <div class="nav-links">
                        <div class="nav-previous"><a href="http://localhost/business-bliss/2019/03/07/est-qui-dolorem/"
                                                     rel="prev">Previous</a></div>
                        <div class="nav-next"><a href="http://localhost/business-bliss/2019/03/07/amet-consectetur/"
                                                 rel="next">Next</a></div>
                    </div>
                </nav>
                <div id="comments" class="comments-area">
                    <h2 class="comments-title">
                        0 thoughts on “<span>Latest Blog</span>” </h2>

                    <ol class="comment-list">
                        <li id="comment-2" class="comment even thread-even depth-1">
                            <article id="div-comment-2" class="comment-body">
                                <footer class="comment-meta">
                                    <div class="comment-author vcard">
                                        <img alt=""
                                             src="http://1.gravatar.com/avatar/4a9353736ea4c612796cc49b5653eed5?s=32&amp;d=mm&amp;r=g"
                                             srcset="http://1.gravatar.com/avatar/4a9353736ea4c612796cc49b5653eed5?s=64&amp;d=mm&amp;r=g 2x"
                                             class="avatar avatar-32 photo" height="32" width="32"> <b class="fn">fsdfgsdfgdsf</b>
                                        <span class="says">says:</span></div><!-- .comment-author -->

                                    <div class="comment-metadata">
                                        <a href="http://localhost/business-bliss/2019/03/07/latest-blog/#comment-2">
                                            <time datetime="2019-03-14T09:56:59+00:00">
                                                March 14, 2019 at 9:56 am
                                            </time>
                                        </a>
                                    </div><!-- .comment-metadata -->

                                    <em class="comment-awaiting-moderation">Your comment is awaiting moderation. This is
                                        a preview, your comment will be visible after it has been approved.</em>
                                </footer><!-- .comment-meta -->

                                <div class="comment-content">
                                    <p>asdfasdasdfsdf</p>
                                </div><!-- .comment-content -->

                                <div class="reply"><a rel="nofollow" class="comment-reply-link"
                                                      href="/business-bliss/2019/03/07/latest-blog/?replytocom=2#respond"
                                                      data-commentid="2" data-postid="11"
                                                      data-belowelement="div-comment-2" data-respondelement="respond"
                                                      aria-label="Reply to fsdfgsdfgdsf">Reply</a></div>
                            </article><!-- .comment-body -->
                        </li><!-- #comment-## -->
                    </ol>

                    <div id="respond" class="comment-respond">
                        <h3 id="reply-title" class="comment-reply-title">Leave a Reply
                            <small><a rel="nofollow" id="cancel-comment-reply-link"
                                      href="/business-bliss/2019/03/07/latest-blog/#respond" style="display:none;">Cancel
                                    reply</a></small>
                        </h3>
                        <form action="http://localhost/business-bliss/wp-comments-post.php" method="post"
                              id="commentform" class="comment-form" novalidate="">
                            <p class="comment-notes"><span
                                        id="email-notes">Your email address will not be published.</span> Required
                                fields are marked <span class="required">*</span></p>
                            <p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment"
                                                                                                           name="comment"
                                                                                                           cols="45"
                                                                                                           rows="8"
                                                                                                           maxlength="65525"
                                                                                                           required="required"></textarea>
                            </p>
                            <p class="comment-form-author"><label for="author">Name <span
                                            class="required">*</span></label> <input id="author" name="author"
                                                                                     type="text" value="" size="30"
                                                                                     maxlength="245"
                                                                                     required="required"></p>
                            <p class="comment-form-email"><label for="email">Email <span
                                            class="required">*</span></label> <input id="email" name="email"
                                                                                     type="email" value="" size="30"
                                                                                     maxlength="100"
                                                                                     aria-describedby="email-notes"
                                                                                     required="required"></p>
                            <p class="comment-form-url"><label for="url">Website</label> <input id="url" name="url"
                                                                                                type="url" value=""
                                                                                                size="30"
                                                                                                maxlength="200"></p>
                            <p class="form-submit"><input name="submit" type="submit" id="submit" class="submit"
                                                          value="Post Comment"> <input type="hidden"
                                                                                       name="comment_post_ID" value="11"
                                                                                       id="comment_post_ID">
                                <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                            </p></form>
                    </div><!-- #respond -->

                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-12">
               <?php include 'sidebar.php' ?>
            </div>
        </div>
    </div>
</section>


<?php include 'footer.php'; ?>
