<?php include 'header.php'; ?>
<section class="breadcrumb" style=" background: url('assets/images/breadcrumbs.jpg') center ;">
    <div class="thin_layer" style="background: #000; opacity: 0.7"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-sm-12 col-xs-12 breadcrumb_wrapper">
                <h1 class="entry-title">Our Team</h1>
                <nav id="breadcrumb" class="fitness-park-breadcrumb">
                    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"
                         itemprop="breadcrumb">
                        <ul class="trail-items" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <meta name="numberOfItems" content="2">
                            <meta name="itemListOrder" content="Ascending">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-begin"><a href="#" rel="home"
                                                                  itemprop="item"><span itemprop="name">Home</span></a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-end"><span itemprop="item"><span itemprop="name">Our Team</span></span>
                                <meta itemprop="position" content="2">
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_team">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="box">
                    <figure><img src="assets/images/team-1.png" alt="" width="800" height="800">
                        <div class="overlay"></div>
                        <ul class="cons_light_team_social ">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </figure>
                    <div class="team-bottom">
                        <h4><a href="#">Daisy Chan</a></h4>
                        <span>Site Supervisor</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="box">
                    <figure><img src="assets/images/team-2.png" alt="" width="800" height="800">
                        <div class="overlay"></div>
                        <ul class="cons_light_team_social ">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </figure>
                    <div class="team-bottom">
                        <h4><a href="#">Ducky Bucky</a></h4>
                        <span>Civil Engineer</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="box">
                    <figure><img src="assets/images/team-1.png" alt="" width="800" height="800">
                        <div class="overlay"></div>
                        <ul class="cons_light_team_social ">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </figure>
                    <div class="team-bottom">
                        <h4><a href="#">Ducky Bucky</a></h4>
                        <span>Civil Engineer</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<div class="cons_light_client_logo">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme client_logo">
                    <div class="item">
                        <img src="assets/images/client-logo-1.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-2.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-3.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-4.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-6.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-7.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-8.png" alt="" class="img-fluid ">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php include 'footer.php'; ?>
