<?php include 'header.php'; ?>
<section class="breadcrumb" style=" background: url('assets/images/breadcrumbs.jpg') center ;">
    <div class="thin_layer" style="background: #000; opacity: 0.7"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-sm-12 col-xs-12 breadcrumb_wrapper">
                <h1 class="entry-title">Search</h1>
                <nav id="breadcrumb" class="fitness-park-breadcrumb">
                    <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"
                         itemprop="breadcrumb">
                        <ul class="trail-items" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <meta name="numberOfItems" content="2">
                            <meta name="itemListOrder" content="Ascending">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-begin"><a href="#" rel="home"
                                                                  itemprop="item"><span itemprop="name">Home</span></a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                class="trail-item trail-end"><span itemprop="item"><span itemprop="name">Search</span></span>
                                <meta itemprop="position" content="2">
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>


<section class="cons_light_article_list">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
                <div class="blog-list-box">
                    <div class="blog-list-content">
                        <h4><a href="single.php">Photoshop CC 2017</a></h4>
                        <div class="blog-list-text-info">
                            <span>By: <span>M S Nawaz</span></span>
                            <span>Date: <span>20.5.15</span></span>
                        </div>
                        <p>There are many variations of sages of Lorem Ipsum available, but the mrity have suffered
                            alteration in some orm, by injected humo ur,There are many but the mri have suffered
                            alteration in some </p>
                        <div class="blog-list-meta">
                            <ul class="single-item-comment-view">
                                <li><i class="fas fa-comment"></i>59</li>
                                <li><i class="fas fa-eye"></i>19</li>
                            </ul>
                        </div>
                        <div class="button-bottom">
                            <a href="single.php" class="btn_yellow link">Learn Now</a>
                        </div>

                    </div>
                </div>
                <div class="blog-list-box">
                    <div class="blog-image">
                        <a href="" class="link-icon"><img src="assets/images/slider-1.png" alt=""></a>
                    </div>
                    <div class="blog-list-content">
                        <h4><a href="single.php">Photoshop CC 2017</a></h4>
                        <div class="blog-list-text-info">
                            <span>By: <span>M S Nawaz</span></span>
                            <span>Date: <span>20.5.15</span></span>
                        </div>
                        <p>There are many variations of sages of Lorem Ipsum available, but the mrity have suffered
                            alteration in some orm, by injected humo ur,There are many but the mri have suffered
                            alteration in some </p>
                        <div class="blog-list-meta">
                            <ul class="single-item-comment-view">
                                <li><i class="fas fa-comment"></i>59</li>
                                <li><i class="fas fa-eye"></i>19</li>
                            </ul>
                        </div>
                        <div class="button-bottom">
                            <a href="single.php" class="btn_yellow link">Learn Now</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php' ?>