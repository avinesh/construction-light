<?php include 'header-1.php'; ?>
<div class="cons_light_slider">
    <div class="owl-carousel owl-theme main_slider">
        <div class="item">
            <img src="assets/images/slider-1.png" alt="logo"/>
            <div class="thin_layer" style="background: #000; opacity: 0.6"></div>
            <div class="container">
                <div class="slider-caption">
                    <h2 data-animation-in="fadeInUp">SIMPLE MODERN & CLEAN</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et tempor incididunt ut labore</p>
                    <a href="#" class="link btn_yellow">Join Now</a>
                </div>
            </div>
        </div>
        <div class="item" style="background: url('assets/images/slider-2.png');">
            <img src="assets/images/slider-2.png" alt="logo"/>
            <div class="thin_layer" style="background: #000; opacity: 0.6"></div>
            <div class="container">
                <div class="slider-caption">
                    <h2 data-animation-in="fadeInUp">SIMPLE MODERN & CLEAN</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et tempor incididunt ut labore</p>
                    <a href="#" class="link btn_yellow">Contact Us</a>
                </div>
            </div>
        </div>
    </div>

</div>

<section class="cons_light_aboutus_front cons_light_progres_bar_skill">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6  col-sm-12">
                <h2 class="section-title">
                    Better Solutions For <span>Borrowers
And Investors</span>
                </h2>
                <p>
                    Nulla ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus. Aliquam scelerisque matt.
                    Morbi id dolor turpis. Prae sent volutpat scelerisque lectus a condimentum. Vestibulum sit amet
                    fermentum lacus.
                </p>
                <div class="progres_bar">
                    <div class="skill">
                        <p>Kitchen Renovation</p>
                        <div class="skill-bar wow slideInLeft animated" style="width: 95%">
                            <span class="skill-count">95%</span>
                        </div>
                    </div>
                    <div class="skill">
                        <p>Home</p>
                        <div class="skill-bar  wow slideInLeft animated" style="width: 85%">
                            <span class="skill-count">85%</span>
                        </div>
                    </div>
                    <div class="skill">
                        <p>office</p>
                        <div class="skill-bar  wow slideInLeft animated" style="width: 90%">
                            <span class="skill-count">90%</span>
                        </div>
                    </div>
                    <div class="skill">
                        <p>Swimming Pool</p>
                        <div class="skill-bar  wow slideInLeft animated" style="width: 96%">
                            <span class="skill-count">96%</span>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 ">
                <img src="assets/images/about.png" alt="about "/>
            </div>
        </div>
    </div>
</section>


<section class="cons_light_services_layout_two">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2 class="section-title">
                    Our <span>Services</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-xs-12 services-list">
                <div class="box">
                    <a href="#">
                        <div class="icon-box">
                            <i class="fas fa-feather-alt"></i>
                        </div>
                        <div class="service_content">
                            <h3>Development &amp; CMS</h3>
                            <p>Direct mailing hypothese channels return on invest.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 services-list">
                <div class="box">
                    <a href="#">
                        <div class="icon-box">
                            <i class="fab fa-angellist"></i>
                        </div>
                        <div class="service_content">
                            <h3>Development &amp; CMS</h3>
                            <p>Direct mailing hypothese channels return on invest.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 services-list">
                <div class="box">
                    <a href="#">
                        <div class="icon-box">
                            <i class="fab fa-cloudversify"></i>
                        </div>
                        <div class="service_content">
                            <h3>Development &amp; CMS</h3>
                            <p>Direct mailing hypothese channels return on invest.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 services-list">
                <div class="box">
                    <a href="#">
                        <div class="icon-box">
                            <i class="fab fa-cloudversify"></i>
                        </div>
                        <div class="service_content">
                            <h3>Development &amp; CMS</h3>
                            <p>Direct mailing hypothese channels return on invest.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 services-list">
                <div class="box">
                    <a href="#">
                        <div class="icon-box">
                            <i class="fab fa-pied-piper-hat"></i>
                        </div>
                        <div class="service_content">
                            <h3>Development &amp; CMS</h3>
                            <p>Direct mailing hypothese channels return on invest.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-xs-12 services-list">
                <div class="box">
                    <a href="#">
                        <div class="icon-box">
                            <i class="fas fa-water"></i>
                        </div>
                        <div class="service_content">
                            <h3>Development &amp; CMS</h3>
                            <p>Direct mailing hypothese channels return on invest.</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="video" style="background:url('assets/images/video_bg.png') no-repeat  bottom center ; ">
    <div class="thin_layer" style="background: #000; opacity: 0.6"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="text-center mt-5">
                    We understand that projects represent not only buildings, but the plans for the future of our
                    clients
                </h2>
                <p class="text-center mt-5">
                    <a href="#popup-video" target="_blank" class="popup-video-link link btn_border">Why Construction
                        Light?</a>
                </p>
                <div id="popup-video" class="mfp-hide embed-responsive embed-responsive-21by9">
                    <iframe class="embed-responsive-item" width="854" height="480"
                            src="https://www.youtube.com/embed/pMdJNNABwfo" frameborder="0"
                            allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
</section>


<!--Corporate Portfolio Section 1-->
<section class="cons_light-portfolio">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <h2 class="section-title">
                    Recent <span>Works</span>
                </h2>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-12 col-xs-12 text-center mb-75">
                <div class="cons_light-isotop-filter-1 isotop-filter">
                    <button class="active" data-filter="*">All Work</button>
                    <button data-filter=".branding">Branding</button>
                    <button data-filter=".mobile-app">Mobile App</button>
                    <button data-filter=".web">Web</button>
                    <button data-filter=".photography">Photography</button>
                    <button data-filter=".illustration">Illustration</button>
                </div>
            </div>

        </div>

        <div class="cons_light-isotop-grid-1 isotop-grid isotop-grid-masonry row no-gutters">
            <div class="cons_light-isotop-item-1 isotop-item branding web col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/feature-1.png" alt="portfolio">
                    <span class="portfolio-content">
                       <i class="far fa-image"></i>
                        <span class="title">Branding</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item mobile-app photography col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/feature-2.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Mobile App</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item illustration web col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/feature-3.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item photography branding col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/services-1.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item illustration mobile-app col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/services-2.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item branding web col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/services-3.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item photography branding col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/team-1.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item illustration mobile-app col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/team-2.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Art Illustration</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item illustration web col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/team-2.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

        </div>

    </div>
</section>
<section class="cons_light_counter" data-parallax="scroll" data-image-src="assets/images/counter_bg.png">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 ">
                <h2>25 years of experience</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="box">
                    <h3 class="counter">13200</h3>
                    <h4>Project Done</h4>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="box">
                    <h3 class="counter">56200</h3>
                    <h4>Awards</h4>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="box">
                    <h3 class="counter">5200</h3>
                    <h4>Happy Clients</h4>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="box">
                    <h3 class="counter">6300</h3>
                    <h4>Employees</h4>
                </div>
            </div>


        </div>
    </div>
</section>

<section class="cons_light_team_layout_two">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2 class="section-title">
                    Our <span>Team</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box">
                    <figure>
                        <img src="assets/images/testimonial-3.jpg" alt="" width="800" height="800">
                    </figure>

                    <h4><a href="#">Daisy Chan</a></h4>
                    <span>Site Supervisor</span>

                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
                        deleniti atque corrupti quos dolores et quas molestias excepturi </p>

                    <ul class="cons_light_team_social">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box">
                    <figure>
                        <img src="assets/images/testimonial-4.jpg" alt="" width="800" height="800">
                    </figure>

                    <h4><a href="#">Daisy Chan</a></h4>
                    <span>Site Supervisor</span>

                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
                        deleniti atque corrupti quos dolores et quas molestias excepturi </p>

                    <ul class="cons_light_team_social">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box">
                    <figure>
                        <img src="assets/images/testimonial-1.jpg" alt="" width="800" height="800">
                    </figure>

                    <h4><a href="#">Daisy Chan</a></h4>
                    <span>Site Supervisor</span>

                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
                        deleniti atque corrupti quos dolores et quas molestias excepturi </p>

                    <ul class="cons_light_team_social">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_testimonial_layout_two"
         style="background: url('assets/images/testimonial-bg.jpg') no-repeat bottom center;">
    <div class="thin_layer" style="background: #000; opacity: 0.8"></div>
    <div class="container">
        <div class="row">
            <div class="owl-carousel owl-theme testimonial_slider_two">
                <div class="item">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="testimonial_client">
                            <div class="testimonial_client_image">
                                <img src="assets/images/testimonial-2.jpg" alt="" class="img-fluid ">
                            </div>
                            <div class="testimonial_client_detail">
                                <h3>John Martin</h3>
                                <h4>Senior Developer</h4>
                            </div>
                        </div>

                        <div class="testimonial_client_content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                                suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                                consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="testimonial_client">
                            <div class="testimonial_client_image">
                                <img src="assets/images/testimonial-1.jpg" alt="" class="img-fluid ">
                            </div>
                            <div class="testimonial_client_detail">
                                <h3>John Martin</h3>
                                <h4>Senior Developer</h4>
                            </div>
                        </div>

                        <div class="testimonial_client_content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                                suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                                consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="testimonial_client">
                            <div class="testimonial_client_image">
                                <img src="assets/images/testimonial-5.jpg" alt="" class="img-fluid ">
                            </div>
                            <div class="testimonial_client_detail">
                                <h3>John Martin</h3>
                                <h4>Senior Developer</h4>
                            </div>
                        </div>

                        <div class="testimonial_client_content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                                suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                                consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_blog-list-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2 class="section-title">
                    Our <span>Blog</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="blog-list-box">
                    <figure>
                        <img src="assets/images/blog-1.jpg" alt="blog"/>
                    </figure>
                    <div class="blog-list-content">
                        <h4><a href="#">Photoshop CC 2017</a></h4>
                        <div class="blog-list-text-info">
                            <span>By: <span>M S Nawaz</span></span>
                            <span>Date: <span>20.5.15</span></span>
                        </div>
                        <p>There are many variations of sages of Lorem Ipsum available, but the mrity have suffered
                            alteration in some orm, by injected humo ur,There are many but the mri have suffered
                            alteration in some </p>
                        <div class="blog-list-meta">
                            <ul class="single-item-comment-view">
                                <li><i class="fas fa-comment"></i>59</li>
                                <li><i class="fas fa-eye"></i>19</li>
                            </ul>
                        </div>
                        <div class="button-bottom">
                            <a href="#" class="btn_yellow link">Learn Now</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="blog-list-box">
                    <figure>
                        <img src="assets/images/blog-2.jpg" alt="blog"/>
                    </figure>
                    <div class="blog-list-content">
                        <h4><a href="#">Photoshop CC 2017</a></h4>
                        <div class="blog-list-text-info">
                            <span>By: <span>M S Nawaz</span></span>
                            <span>Date: <span>20.5.15</span></span>
                        </div>
                        <p>There are many variations of sages of Lorem Ipsum available, but the mrity have suffered
                            alteration in some orm, by injected humo ur,There are many but the mri have suffered
                            alteration in some </p>
                        <div class="blog-list-meta">
                            <ul class="single-item-comment-view">
                                <li><i class="fas fa-comment"></i>59</li>
                                <li><i class="fas fa-eye"></i>19</li>
                            </ul>
                        </div>
                        <div class="button-bottom">
                            <a href="#" class="btn_yellow link">Learn Now</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="blog-list-box">
                    <figure>
                        <img src="assets/images/blog-3.jpg" alt="blog"/>
                    </figure>
                    <div class="blog-list-content">
                        <h4><a href="#">Photoshop CC 2017</a></h4>
                        <div class="blog-list-text-info">
                            <span>By: <span>M S Nawaz</span></span>
                            <span>Date: <span>20.5.15</span></span>
                        </div>
                        <p>There are many variations of sages of Lorem Ipsum available, but the mrity have suffered
                            alteration in some orm, by injected humo ur,There are many but the mri have suffered
                            alteration in some </p>
                        <div class="blog-list-meta">
                            <ul class="single-item-comment-view">
                                <li><i class="fas fa-comment"></i>59</li>
                                <li><i class="fas fa-eye"></i>19</li>
                            </ul>
                        </div>
                        <div class="button-bottom">
                            <a href="#" class="btn_yellow link">Learn Now</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_client_logo_layout_two">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2 class="section-title">
                    Our <span>Clients</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme client_logo">
                    <div class="item">
                        <div class="box">
                            <img src="assets/images/client-logo-1.png" alt="" class="img-fluid ">
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <img src="assets/images/client-logo-2.png" alt="" class="img-fluid ">
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <img src="assets/images/client-logo-3.png" alt="" class="img-fluid ">
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <img src="assets/images/client-logo-4.png" alt="" class="img-fluid ">
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <img src="assets/images/client-logo-6.png" alt="" class="img-fluid ">
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <img src="assets/images/client-logo-7.png" alt="" class="img-fluid ">
                        </div>
                    </div>
                    <div class="item">
                        <div class="box">
                            <img src="assets/images/client-logo-8.png" alt="" class="img-fluid ">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<?php include 'footer.php'; ?>
