<?php include 'header.php'; ?>
<div class="cons_light_slider custom-modify-slider">
    <div class="owl-carousel owl-theme main_slider">
        <div class="item" style="background: url('assets/images/slider-1.png');">
            <div class="thin_layer" style="background: #000; opacity: 0.8"></div>
            <div class="container slider-text-inner">
                <div class="slider-caption">
                    <h2 data-animation-in="fadeInUp">SIMPLE MODERN & CLEAN</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore
                        et</p>
                    <a href="#" class="link btn_yellow">Join Now</a>
                </div>
            </div>
        </div>
        <div class="item" style="background: url('assets/images/slider-2.png');">
            <div class="thin_layer" style="background: #000; opacity: 0.8"></div>
            <div class="container slider-text-inner">
                <div class="slider-caption">
                    <h2 data-animation-in="fadeInUp">SIMPLE MODERN & CLEAN</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore
                        et</p>
                    <a href="#" class="link btn_yellow">Join Now</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row no-gutters">
            <div class="get_a_quote">
                <h3>Get a Free Quote!</h3>
                <p class="sub-title">Fill this form to get quick call.</p>
                <div role="form" class="wpcf7" id="wpcf7-f59-o1" lang="en-US" dir="ltr">
                    <div class="screen-reader-response"></div>
                    <form action="/fitness-park/#wpcf7-f59-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                        <div style="display: none;">
                            <input type="hidden" name="_wpcf7" value="59">
                            <input type="hidden" name="_wpcf7_version" value="5.1.1">
                            <input type="hidden" name="_wpcf7_locale" value="en_US">
                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f59-o1">
                            <input type="hidden" name="_wpcf7_container_post" value="0">
                            <input type="hidden" name="g-recaptcha-response" value="">
                        </div>
                        <p><label>
                                <span class="wpcf7-form-control-wrap your-name">
                                    <input type="text" class="wpcf7-form-control wpcf7-text" placeholder="Your Name">
                                </span>
                            </label>
                        </p>
                        <p><label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="email" name="your-email" class="wpcf7-form-control wpcf7-text"
                                           placeholder="Your Email (required)">
                                </span>
                            </label>
                        </p>
                        <p><label>
                                <span class="wpcf7-form-control-wrap your-subject">
                                    <input type="text" name="your-subject" value=""
                                           class="wpcf7-form-control wpcf7-text" placeholder="Subject">
                                </span>
                            </label>
                        </p>
                        <p><label>
                                <span class="wpcf7-form-control-wrap your-message">
                                    <textarea name="your-message" class="wpcf7-form-control wpcf7-textarea"
                                              placeholder="Message"></textarea>
                                </span>
                            </label></p>
                        <p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit"><span
                                    class="ajax-loader"></span></p>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="cons_light_feature">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12 col-xs-12 feature-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/feature-1.png" class=""/>
                        </figure>
                        <div class="bottom-content">
                            <div class="icon-box">
                                <i class="fas fa-feather-alt"></i>
                            </div>
                            <h3>Development &amp; CMS</h3>
                            <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                                return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                                hypothese channels return on invest.</p>
                        </div>

                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 col-xs-12 feature-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/feature-2.png" class=""/>
                        </figure>
                        <div class="bottom-content">
                            <div class="icon-box">
                                <i class="fas fa-cogs"></i>
                            </div>
                            <h3>Development &amp; CMS</h3>
                            <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                                return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                                hypothese channels return on invest.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 col-xs-12 feature-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/feature-3.png" class=""/>
                        </figure>
                        <div class="bottom-content">
                            <div class="icon-box">
                                <i class="fas fa-brain"></i>
                            </div>
                            <h3>Development &amp; CMS</h3>
                            <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                                return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                                hypothese channels return on invest.</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="call_to_action" style="background:url('assets/images/call_to_action.png') no-repeat  bottom center cover">
    <div class="thin_layer" style="background: #000; opacity: 0.8"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-md-12 col-sm-12">
                <div class="call_to_action_inner wow fadeIn ">
                    <h2>Ready To Work with Our Company?</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in condimentum risus. Fusce
                        rutrum, leo in elementum sodales, magna eros vehicula ante, eget semper mi lectus nec ipsum.</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-12 col-sm-12 mt-5">
                <a href="" class="link btn_border ">Call Us Now</a>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_aboutus_front">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <img src="assets/images/about-1.png" alt="" width="670" height="500">
            </div>
            <div class="col-lg-5 col-md-6  col-sm-12">
                <h2 class="section-title">
                    Better Solutions For Borrowers <br> <span>And Investors In Middle Market Credit</span>
                </h2>
                <p>
                    Nulla ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus. Aliquam scelerisque matt.
                    Morbi id dolor turpis. Prae sent volutpat scelerisque lectus a condimentum. Vestibulum sit amet
                    fermentum lacus.<br><br>Nulla ut fringilla dui. Nullam sit amet pulvinar velit, at rutrum lacus.
                    Aliquam scelerisque matt.
                    Morbi id dolor turpis. Praesent volutpat scelerisque lectus a condimentum. Vestibulum sit amet
                    fermentum lacus.
                </p>
                <div>
                    <a href="#" class="link btn_yellow">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <h2 class="section-title">
                    Our <span>Services</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-12 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-1.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Home Construction</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Lerarn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-2.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Handyman service</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Lerarn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-3.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Roofing </h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Lerarn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-4.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Development &amp; CMS</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Lerarn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-5.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Kitchen renovation</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Lerarn more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 col-xs-12 service-list">
                <div class="box">
                    <a href="#">
                        <figure>
                            <img src="assets/images/services-6.png" class="">
                        </figure>
                    </a>
                    <div class="bottom-content">
                        <a href="#"><h3>Office construction</h3></a>
                        <p>Direct mailing hypothese channels return on invest. Direct mailing hypothese channels
                            return on invest.Direct mailing hypothese channels return on invest. Direct mailing
                            hypothese channels return on invest.</p>
                        <a href="#" class="btn_yellow link btn_light">Lerarn more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="video" style="background:url('assets/images/video_bg.png')  no-repeat  bottom center ">
    <div class="thin_layer" style="background: #000; opacity: 0.7"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="text-center mt-5">
                    We understand that projects represent not only buildings, but the plans for the future of our
                    clients
                </h2>
                <p class="text-center mt-5">
                    <a href="#popup-video" target="_blank" class="popup-video-link link btn_border">Why Construction
                        Light?</a>
                </p>
                <div id="popup-video" class="mfp-hide embed-responsive embed-responsive-21by9">
                    <iframe class="embed-responsive-item" width="854" height="480"
                            src="https://www.youtube.com/embed/pMdJNNABwfo" frameborder="0"
                            allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
</section>


<!--Corporate Portfolio Section 1-->
<section class="cons_light-portfolio">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12">
                <h2 class="section-title">
                    Recent <span>Works</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 text-center mb-75">
                <div class="cons_light-isotop-filter-1 isotop-filter">
                    <button class="active" data-filter="*">All Work</button>
                    <button data-filter=".branding">Branding</button>
                    <button data-filter=".mobile-app">Mobile App</button>
                    <button data-filter=".web">Web</button>
                    <button data-filter=".photography">Photography</button>
                    <button data-filter=".illustration">Illustration</button>
                </div>
            </div>
        </div>

        <div class="cons_light-isotop-grid-1 isotop-grid isotop-grid-masonry row no-gutters">
            <div class="cons_light-isotop-item-1 isotop-item branding web col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/feature-1.png" alt="portfolio">
                    <span class="portfolio-content">
                       <i class="far fa-image"></i>
                        <span class="title">Branding</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item mobile-app photography col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/feature-2.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Mobile App</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item illustration web col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/feature-3.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item photography branding col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/services-1.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item illustration mobile-app col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/services-2.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item branding web col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/services-3.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item photography branding col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/team-1.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item illustration mobile-app col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/team-2.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Art Illustration</span>
                    </span>
                </a>
            </div>

            <div class="cons_light-isotop-item-1 isotop-item illustration web col-md-4 col-sm-6 col-xs-12 no-padding">
                <a href="#">
                    <img src="assets/images/team-2.png" alt="portfolio">
                    <span class="portfolio-content">
                        <i class="far fa-image"></i>
                        <span class="title">Web Design</span>
                    </span>
                </a>
            </div>

        </div>

    </div>
</section>

<section class="cons_light_counter" data-parallax="scroll" data-image-src="assets/images/counter_bg.png">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 ">
                <h2>25 years of experience</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="box">
                    <h3 class="counter">13200</h3>
                    <h4>Project Done</h4>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="box">
                    <h3 class="counter">56200</h3>
                    <h4>Awards</h4>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="box">
                    <h3 class="counter">5200</h3>
                    <h4>Happy Clients</h4>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 ">
                <div class="box">
                    <h3 class="counter">6300</h3>
                    <h4>Employees</h4>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_blog-list-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2 class="section-title">
                    Our <span>Blog</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="blog-list-box">
                    <figure>
                        <img src="assets/images/blog-1.jpg" alt="blog"/>
                    </figure>
                    <div class="blog-list-content">
                        <h4><a href="#">Photoshop CC 2017</a></h4>
                        <div class="blog-list-text-info">
                            <span>By: <span>M S Nawaz</span></span>
                            <span>Date: <span>20.5.15</span></span>
                        </div>
                        <p>There are many variations of sages of Lorem Ipsum available, but the mrity have suffered
                            alteration in some orm, by injected humo ur,There are many but the mri have suffered
                            alteration in some </p>
                        <div class="blog-list-meta">
                            <ul class="single-item-comment-view">
                                <li><i class="fas fa-comment"></i>59</li>
                                <li><i class="fas fa-eye"></i>19</li>
                            </ul>
                        </div>
                        <div class="button-bottom">
                            <a href="#" class="btn_yellow link">Learn Now</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="blog-list-box">
                    <figure>
                        <img src="assets/images/blog-2.jpg" alt="blog"/>
                    </figure>
                    <div class="blog-list-content">
                        <h4><a href="#">Photoshop CC 2017</a></h4>
                        <div class="blog-list-text-info">
                            <span>By: <span>M S Nawaz</span></span>
                            <span>Date: <span>20.5.15</span></span>
                        </div>
                        <p>There are many variations of sages of Lorem Ipsum available, but the mrity have suffered
                            alteration in some orm, by injected humo ur,There are many but the mri have suffered
                            alteration in some </p>
                        <div class="blog-list-meta">
                            <ul class="single-item-comment-view">
                                <li><i class="fas fa-comment"></i>59</li>
                                <li><i class="fas fa-eye"></i>19</li>
                            </ul>
                        </div>
                        <div class="button-bottom">
                            <a href="#" class="btn_yellow link">Learn Now</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="blog-list-box">
                    <figure>
                        <img src="assets/images/blog-3.jpg" alt="blog"/>
                    </figure>
                    <div class="blog-list-content">
                        <h4><a href="#">Photoshop CC 2017</a></h4>
                        <div class="blog-list-text-info">
                            <span>By: <span>M S Nawaz</span></span>
                            <span>Date: <span>20.5.15</span></span>
                        </div>
                        <p>There are many variations of sages of Lorem Ipsum available, but the mrity have suffered
                            alteration in some orm, by injected humo ur,There are many but the mri have suffered
                            alteration in some </p>
                        <div class="blog-list-meta">
                            <ul class="single-item-comment-view">
                                <li><i class="fas fa-comment"></i>59</li>
                                <li><i class="fas fa-eye"></i>19</li>
                            </ul>
                        </div>
                        <div class="button-bottom">
                            <a href="#" class="btn_yellow link">Learn Now</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cons_light_testimonial" style="background: url('assets/images/testimonial-bg.jpg') no-repeat bottom center cover;">
    <div class="thin_layer" style="background: #000; opacity: 0.8"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="owl-carousel owl-theme testimonial_slider">
                    <div class="item">
                        <div class="client-img">
                            <img src="assets/images/testimonial-1.jpg" alt="" class="img-fluid ">
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                            suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                            consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>

                        <div class="client-text">
                            <h3>John Martin</h3>
                            <h4>Senior Developer</h4>
                        </div>
                    </div>
                    <div class="item">
                        <div class="client-img">
                            <img src="assets/images/testimonial-2.jpg" alt="" class="img-fluid ">
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium illum
                            suscipit, quam quas fuga illo cupiditate labore eveniet, officia in modi eum
                            consequuntur quis alias delectus hic? Delectus, molestias eaque.</p>

                        <div class="client-text">
                            <h3>John Martin</h3>
                            <h4>Senior Developer</h4>
                        </div>
                    </div>


                </div>
            </div>


        </div>
    </div>
</section>
<section class="cons_light_team">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2 class="section-title">
                    Our <span>Team</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box">
                    <figure><img src="assets/images/team-1.png" alt="" width="800" height="800">
                        <div class="overlay"></div>
                        <ul class="cons_light_team_social ">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </figure>
                    <div class="team-bottom">
                        <h4><a href="#">Daisy Chan</a></h4>
                        <span>Site Supervisor</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box">
                    <figure><img src="assets/images/team-2.png" alt="" width="800" height="800">
                        <div class="overlay"></div>
                        <ul class="cons_light_team_social ">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </figure>
                    <div class="team-bottom">
                        <h4><a href="#">Ducky Bucky</a></h4>
                        <span>Civil Engineer</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box">
                    <figure><img src="assets/images/team-1.png" alt="" width="800" height="800">
                        <div class="overlay"></div>
                        <ul class="cons_light_team_social ">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </figure>
                    <div class="team-bottom">
                        <h4><a href="#">Ducky Bucky</a></h4>
                        <span>Civil Engineer</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="cons_light_client_logo">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme client_logo">
                    <div class="item">
                        <img src="assets/images/client-logo-1.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-2.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-3.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-4.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-6.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-7.png" alt="" class="img-fluid ">
                    </div>
                    <div class="item">
                        <img src="assets/images/client-logo-8.png" alt="" class="img-fluid ">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php include 'footer.php'; ?>
