<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Construction Light - Wordpress Theme</title>

    <link href="assets/library/font-awesome/css/fontawsome.css" type="text/css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,800"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700" rel="stylesheet">

    <link href="assets/library/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    <link href="assets/library/owlcarousel/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="assets/library/owlcarousel/css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="assets/library/prettyPhoto/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="assets/library/magnific-popup/magnefic.min.css" rel="stylesheet" type="text/css">
    <link href="assets/library/jqueryUI/jquery.ui.css" rel="stylesheet" type="text/css">
    <link href="style.css" type="text/css" rel="stylesheet"/>
    <link href="responsive.css" type="text/css" rel="stylesheet"/>
</head>

<body>
<div class="ss-content">
    <span class="ss-close"><i class="ss-close fas fa-times"></i></span>

    <div class="ssc-inner">
        <form>
            <input type="text" placeholder="Type Search text here...">
            <button type="submit"><i class="icon-search2"></i></button>
        </form>
    </div>
</div>
<header>
    <div class="cons_light_top_bar">
        <div class="container">
            <div class="top_bar_inner">
                <div class="cons_light_top_info_bar">
                    <ul>
                        <li><a href="tel:"><i class="fas fa-mobile-alt"></i>(+123) 456 7890</a></li>
                        <li><a href="mail:"><i class="fas fa-envelope"></i>addyour@emailhere</a></li>
                        <li><a href=""><i class="fas fa-marker"></i> New York, USA</a></li>
                    </ul>
                </div>
                <ul class="cons_light_top_social_icon right">
                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="cons_light_main_header main-header-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 logo">
                    <div class="site-branding">
                        <!--<a href="index.php" class="custom-logo-link"><img width="290" height="50" src="assets/images/logo.png" class="custom-logo" alt="Construction Light" ></a>-->
                        <h1 class="site-title">
                            <a href="index.php" rel="home">Construction Light</a>
                        </h1>
                        <p class="site-description">Just another WordPress site</p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <nav id="site-navigation" class="main-navigation">
                        <button class="main-menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i
                                    class="fa fa-bars"></i></button>
                        <div class="search_main_menu">
                            <a href="javascript:void(0)"><i class="fas fa-search"></i></a>
                        </div>
                        <div class="main-menu-container-collapse">
                            <ul id="primary-menu" class="menu nav-menu right" aria-expanded="false">
                                <li class="menu-item-has-children"> <a href="#">Home</a>
                                    <ul class="sub-menu">
                                        <li><a href="aboutus.php">About Us</a></li>
                                        <li><a href="service.php">Service</a></li>
                                        <li><a href="team.php">Team</a></li>
                                    </ul>
                                </li>
                                <li><a href="aboutus.php">About Us</a></li>
                                <li><a href="service.php">Service</a></li>
                                <li><a href="team.php">Team</a></li>
                                <li><a href="blog.php">Blog</a></li>
                                <li><a href="contact.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>