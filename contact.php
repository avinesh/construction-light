<?php include 'header.php'; ?>
    <section class="breadcrumb" style=" background: url('assets/images/breadcrumbs.jpg') center ;">
        <div class="thin_layer" style="background: #000; opacity: 0.7"></div>
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-sm-12 col-xs-12 breadcrumb_wrapper">
                    <h1 class="entry-title">Our Team</h1>
                    <nav id="breadcrumb" class="fitness-park-breadcrumb">
                        <div role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs"
                             itemprop="breadcrumb">
                            <ul class="trail-items" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                                <meta name="numberOfItems" content="2">
                                <meta name="itemListOrder" content="Ascending">
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                    class="trail-item trail-begin"><a href="#" rel="home"
                                                                      itemprop="item"><span itemprop="name">Home</span></a>
                                    <meta itemprop="position" content="1">
                                </li>
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"
                                    class="trail-item trail-end"><span itemprop="item"><span
                                                itemprop="name">Our Team</span></span>
                                    <meta itemprop="position" content="2">
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <section class="cons_light_contact">
        <div class="contact_form">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2 class="section-title">
                            Contact Detail
                        </h2>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-12">
                        <div class="contact_detail">
                            <p>Are you looking for professional advice for your new business.Are you looking for
                                professional advice for your new business</p>

                            <div class="contact-icon">
                                <div class="contact-inner">
                                    <a href="#"><i
                                                class="fas fa-map-marker-alt"></i><span>Road-7 old Street, Manhatan</span></a>
                                    <a href="#"><i class="fas fa-phone-volume"></i><span>+132-6565 7654</span></a>
                                    <a href="#"><i class="fas fa-envelope"></i><span>mentose@offices.com</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-12">
                        <div role="form" class="wpcf7" id="wpcf7-f59-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response"></div>
                            <form action="/fitness-park/#wpcf7-f59-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="59">
                                    <input type="hidden" name="_wpcf7_version" value="5.1.1">
                                    <input type="hidden" name="_wpcf7_locale" value="en_US">
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f59-o1">
                                    <input type="hidden" name="_wpcf7_container_post" value="0">
                                    <input type="hidden" name="g-recaptcha-response" value="">
                                </div>
                                <p><label>
                                <span class="wpcf7-form-control-wrap your-name">
                                    <input type="text" class="wpcf7-form-control wpcf7-text" placeholder="Your Name">
                                </span>
                                    </label>
                                </p>
                                <p><label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="email" name="your-email" class="wpcf7-form-control wpcf7-text"
                                           placeholder="Your Email (required)">
                                </span>
                                    </label>
                                </p>
                                <p><label>
                                <span class="wpcf7-form-control-wrap your-subject">
                                    <input type="text" name="your-subject" value=""
                                           class="wpcf7-form-control wpcf7-text" placeholder="Subject">
                                </span>
                                    </label>
                                </p>
                                <p><label>
                                <span class="wpcf7-form-control-wrap your-message">
                                    <textarea name="your-message" class="wpcf7-form-control wpcf7-textarea"
                                              placeholder="Message"></textarea>
                                </span>
                                    </label></p>
                                <p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit"><span
                                            class="ajax-loader"></span></p>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="cons_light_map_responsive">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d386950.6511603643!2d-73.70231446529533!3d40.738882125234106!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNueva+York!5e0!3m2!1ses-419!2sus!4v1445032011908"
                width="600" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>

    </section>
<?php include 'footer.php'; ?>