<footer class="site-footer">
    <div class="sticky-stopper-sidebar"></div>
    <div class="container">
        <div class="row">

            <div class="col-xl-4 col-md-6 col-sm-6">
                <section id="text-2" class="widget widget_about">
                    <div class="aboutwidget">
                        <div class="site-branding">
                            <!-- <a href="#" class="custom-logo-link"><img width="290" height="50" src="assets/images/logo_light.png" class="custom-logo" alt="logo" ></a>-->
                            <h2 class="site-title">
                                <a href="#" rel="home">Construction Light</a>
                            </h2>
                            <p class="site-description">Just another WordPress site</p>
                        </div>
                        <p> Pro multipurpose theme can
                            also be used for other related fitness center.<br><br> CrossFit or needs of health
                            clubs,
                            gymnasiums, spas, and wellness centers any other sport and health-related website.</p>
                    </div>
                </section>
            </div>

            <div class="col-xl-2 col-md-6 col-sm-6">
                <section id="nav_menu-2" class="widget widget_nav_menu">
                    <h3 class="widget-title">Our Services</h3>
                    <div class="menu-useful-links-container">
                        <ul id="menu-useful-links" class="menu">
                            <li id="menu-item-1558"
                                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1558">
                                <a href="http://demo.sparklewpthemes.com/fitnessparkpro/"
                                   aria-current="page">Home</a>
                            </li>
                            <li id="menu-item-1559"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1559"><a
                                        href="#">About Us</a>
                            </li>
                            <li id="menu-item-1561"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1561"><a
                                        href="#">Powerbase
                                    Gym</a></li>
                            <li id="menu-item-1562"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1562"><a
                                        href="#">Personal
                                    Trainer</a></li>
                            <li id="menu-item-1563"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1563"><a
                                        href="#">Build
                                    your body</a></li>

                        </ul>
                    </div>
                </section>
            </div>
            <div class="col-xl-3 col-md-6 col-sm-6">
                <section id="nav_menu-2" class="widget recent-post-widget">
                    <h3 class="widget-title">Recent Posts</h3>
                    <div class="popular-post-widget">
                        <ul>
                            <li>
                                <div class="post-image">
                                    <a href="#">
                                        <img src="assets/images/recent-post-1.jpg" alt="recent post" width="80"
                                             height="80">
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#"><span class="post-title">Best design you can affort</span></a>
                                    <p class="date"><i class="far fa-clock"></i>
                                        Dec 31 </p>
                                </div>
                            </li>
                            <li>
                                <div class="post-image">
                                    <a href="#">
                                        <img src="assets/images/recent-post-2.jpg" alt="recent post" width="80"
                                             height="80">
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#"><span class="post-title">What color says</span></a>
                                    <p class="date"><i class="far fa-clock"></i>
                                        Dec 31 </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>

            <div class="col-xl-3 col-md-6 col-sm-6">
                <section id="text-2" class="widget widget_text">
                    <h3 class="widget-title">Contact Us</h3>
                    <div class="textwidget">
                        <ul class="footer-info">
                            <li>150 Bay Street,New York, NY 7302</li>
                            <li>1 (800) 686-6688</li>
                            <li>admin@kormo.com</li>
                            <li>NewYork City, US</li>
                            <li>Open hours: 8.00-18.00 Mon-Fri</li>
                        </ul>
                    </div>
                </section>
            </div>

        </div>
    </div>
    <div class="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 copyright">
                    <p>© 2019 Construction Light is Powered by <a href="#">Sparkle Themes</a></p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 footer-menu">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Serivces</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>


<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/wow.js"></script>
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/library/counter/jquery.counterup.min.js"></script>
<script src="assets/library/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/library/owlcarousel/js/owl.carousel.min.js"></script>
<script src="assets/js/jquery.isotope.js"></script>
<script src="assets/js/imagesLoaded.js"></script>
<script src="assets/library/prettyPhoto/js/jquery.prettyPhoto.js"></script>
<script src="assets/library/parallex/parallax.min.js"></script>
<script src="assets/library/magnific-popup/magnific-popup.min.js"></script>
<script src="assets/library/jqueryUI/jquery.ui.js"></script>
<script type="text/javascript" src="assets/js/ResizeSensor.min.js"></script>
<script type="text/javascript" src="assets/js/theia-sticky-sidebar.min.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>


</body>
</html>